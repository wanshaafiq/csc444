<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapid Rush Delivery</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"
        id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" rel="stylesheet"
        id="fontawesome-css">
    <link href="CSS/addMenuPage.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
	
    <div class="homepage-bg d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center my-4" style="color: #800080;">
                    <h1>Rapid Rush Delivery</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <div id="formContent" style="width: 65vw; padding: 30px; margin-bottom: 50px;">
                      <div class="row mb-4">
                            <div class="col-7 d-flex">
                            	<div class="col-12 text-left">
                                <a href="userServlet?action=user_orderpending" class="button button1"><i class="fas fa-list-alt"></i> Customer Pending Order List</a>
                                </div>
                                
                                <div class="col-9 text-right">
                                <a href="userServlet?action=logout" class="btn btn-primary">Logout <i class="fas fa-sign-out-alt"></i></a>
                                </div>
                            </div>
                        </div>
 								<div class="col-12 text-left">
                                <a href="userServlet?action=user_orderhistory" class="button button1"><i class="fas fa-history"></i> Customer Order History</a>
                                </div>
                                <br><br>
                           <h3 class="American Typewriter, serif">KFC</h3>
                            <table class="table table-striped menu-food">
                                <thead>
                                    <tr>
                                        <th style="width:70%; text-align: left;">MENU</th>
                                        <th style="width: ; text-align: left;">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${menuList}" var="menu">
                          			<c:if test="${menu.menutype == 'KFC'}">
	                                    <tr align="left"> 
	                                        <td>${menu.menuname}</td>
	                                        <td>RM${menu.menuprice}</td>
	                                        <td><a href="menuServlet?action=edit&menuid=${menu.menuid}"><button class="button">Update Price</button></a></td>  
	                                    </tr>
                                	</c:if>
                          		</c:forEach>                                                   
                                </tbody>
                            </table>
                            
                            <h3 class="American Typewriter, serif">Donut & Coffee</h3>
                            <table class="table table-striped menu-food">
                                <thead>
                                    <tr>
                                        <th style="width:70%; text-align: left;">MENU</th>
                                        <th style="width:; text-align: left;">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <c:forEach items="${menuList}" var="menu">
                          			<c:if test="${menu.menutype == 'Donut'}">
	                                    <tr align="left"> 
	                                        <td>${menu.menuname}</td>
	                                        <td>RM${menu.menuprice}</td>
	                                        <td><button class="button">Update Price</button></td>  
	                                    </tr>
                                	</c:if>
                          		</c:forEach> 
                          		                                                                          
                              </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    <!-- Modal -->
    <div class="modal fade" id="addMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="MenuController?acction=submit_add_menu" method="POST">
                    <div class="modal-body">
                        <label for="price" class="d-block mt-3">Price(RM)</label>
                        <input type="text" name="price" id="price">
						<select name="type" id="type">
                            <option value="0" selected disabled>Select type</option>
                            <option value="foods">KFC</option>
                            <option value="drinks">Donut&Coffee</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>