<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapid Rush Delivery</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"
        id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" rel="stylesheet"
        id="fontawesome-css">
    <link href="CSS/viewCustOrder.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  
</head>

<body>
    <div class="homepage-bg d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center my-4" style="color: #fff;">
                    <h1 class="font-weght-bold" style="text-shadow: 4px 4px rgb(31, 29, 29);">Rapid Rush Delivery</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <div id="formContent" style="width: 65vw; padding: 30px; margin-bottom: 50px;">
                        <div class="row mb-4">
                            <div class="col-6 d-flex justify-content-between">
                            	<div class="col-12 text-left">
                                <a href="menuServlet?action=menu_list" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back to Menu List</a>
                                </div>
                                
                                <div class="col-12 text-right">
                                <a href="userServlet?action=logout" class="btn btn-primary">Logout <i class="fas fa-sign-out-alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div>
                        <h3 align="center">Customer Pending Order List</h3>
                         <table class="table table-striped menu-food">
                          <thead>
                            <tr>
                           		<th style="text-align: center">User ID</th>
                  				<th style="text-align: center">Order ID</th>
                  				<th style="text-align: center">Order Time</th>
                                <th style="text-align: center">Payment Status</th>
                                <th style="text-align: center">Action</th>
                            </tr>
                          </thead>
                           <tbody>
                           	<c:forEach var="order" items="${order}">
	                           <tr>
	                                <td style="text-align: center" >${order.userid}</td>
	                                <td style="text-align: center" >${order.orderid}</td>
	                                <td style="text-align: center" >${order.ordertime}</td>          
	                                <td style="text-align: center" >${order.orderstatus}</td>
	                                <td style="text-align: center" ><a href="userServlet?action=update_pendingstatus&orderid=${order.orderid}"><button class="button button1">Update Status</button></a>
	                           </tr>
	                         </c:forEach>
                           </tbody>             
                          </table>                                                     
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
</body>
</html>