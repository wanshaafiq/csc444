<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapid Rush Delivery</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"
        id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" rel="stylesheet"
        id="fontawesome-css">
    <link href="CSS/Receipt.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  
</head>

<body>
    <div class="homepage-bg d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center my-4" style="color: #fff;">
                    <h1 class="font-weght-bold" style="text-shadow: 4px 4px rgb(31, 29, 29);">Rapid Rush Delivery</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <div id="formContent" style="width: 65vw; padding: 30px; margin-bottom: 50px;">
                        <div class="row mb-4">
                            <div class="col-6 d-flex justify-content-between">
                            	<div class="col-12 text-left">
                                <a href="menuServlet?action=home" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back to Menu</a>
                                </div>
                                <h3>Receipt</h3>
                                <div class="col-10 text-right">
                                <a href="userServlet?action=logout" class="btn btn-primary">Logout <i class="fas fa-sign-out-alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div id="print_receipt">
                        
                        <h3 class="font-weight-bold text-muted mb-4" align="left">Customer Detail</h3>
                         <table class="table table-striped menu-food">
                          <thead>
                            <tr>
                              <th style="width: 45%; text-align: left">Name</th>
                            </tr>
                          </thead>
                           <tbody>
	                           <tr>
	                                <td style="width: 45%; text-align: left" >${users.name}</td>
	                           </tr>
                           </tbody>
                           <thead>
                            <tr>
                            	<th style="width: 45%; text-align: left">Address</th>
                            </tr>
                          </thead>
                           <tbody>
                               <tr>
                               		<td style="width: 45%; text-align: left" >${users.address}</td>
                               </tr>
                           </tbody>                
                          </table>
                          
                            <h3 class="font-weight-bold text-muted mb-4" id="receipt_header" hidden>Receipt</h3>
                             <h3 class="font-weight-bold text-muted mb-4" align="left">Order Detail</h3>
                             <table class="table table-striped menu-food">
                                <thead>
                                    <tr>
                                        <th style="width: 45%; text-align: left;">Name</th>
                                        <th style="width: 10%;">Price</th>
                                        <th style="width: 25%;">Amount</th>
                                        <th style="width: 25%;">Subtotal</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                	<c:forEach items="${OrderList}" var="menu" varStatus="loop">
                                	<c:if test="${OrderList[loop.index].menuqty > 0}">
                                    <tr>
                                        <td style="text-align: left;">${menu.menuname}</td>
                                        <td>${menu.menuprice}</td>
                                        <td>${menu.menuqty}</td>
                                        <td>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${menu.menuprice * OrderList[loop.index].menuqty}"/></td>
                                    </tr>
                                    </c:if>
                                    </c:forEach>						
									<c:choose>
										<c:when test="${order.orderpromo == 'STAYATHOME10' }">
											<c:choose>
												<c:when test="${total >15 }">
													<c:choose>
															<c:when test="${order.ordertime >'22:00' && order.ordertime <'23:59' }">
																<tr>
																	<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
						                                        	<td><h6>RM5</h6></td>
					                                        	</tr>
					                                        	<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
					                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 0.06}"/></h6></td>
					                                    		</tr>
					                                    		<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
					                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 1.06 + 5}"/></h6></td>
					                                    		</tr>
															</c:when>
															<c:otherwise>
																<tr>
																	<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
						                                        	<td><h6>Free of charge</h6></td>
					                                        	</tr>
					                                        	<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
					                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 0.06}"/></h6></td>
					                                    		</tr>
					                                    		<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
					                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 1.06 }"/></h6></td>
					                                    		</tr>
															</c:otherwise>
														</c:choose>
												  </c:when>
												  <c:when test="${total <15 }">
												  		<c:choose>
																<c:when test="${order.ordertime >'22:00' && order.ordertime <'23:59' }">
																	<tr>
																		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
							                                        	<td><h6>RM11</h6></td>
						                                        	</tr>
						                                        	<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
						                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 0.06}"/></h6></td>
						                                    		</tr>
						                                    		<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
						                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 1.06 + 11}"/></h6></td>
						                                    		</tr>
																</c:when>
																<c:otherwise>
																	<tr>
																		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
							                                        	<td><h6>RM6</h6></td>
						                                        	</tr>
						                                        	<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
						                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 0.06}"/></h6></td>
						                                    		</tr>
						                                    		<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
						                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 1.06 + 6}"/></h6></td>
						                                    		</tr>
																</c:otherwise>
														</c:choose>
													</c:when>
											  </c:choose>	  		
										  </c:when>
										  <c:otherwise>
										  		<c:choose>
												  	<c:when test="${total >15 }">
												  		<c:choose>
															<c:when test="${order.ordertime >'22:00' && order.ordertime <'23:59'}">
																<tr>
																	<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
						                                        	<td><h6>RM5</h6></td>
					                                        	</tr>
					                                        	<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
					                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 0.06}"/></h6></td>
					                                    		</tr>
					                                    		<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
					                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 1.06 + 5}"/></h6></td>
					                                    		</tr>
															</c:when>
															<c:otherwise>
																<tr>
																	<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
						                                        	<td><h6>Free of charge</h6></td>
					                                        	</tr>
					                                        	<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
					                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 0.06}"/></h6></td>
					                                    		</tr>
					                                    		<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
					                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 1.06 }"/></h6></td>
					                                    		</tr>
															</c:otherwise>
														 </c:choose>
													  </c:when>		  
													  <c:when test="${total <15 }">
													  		<c:choose>
																<c:when test="${order.ordertime >'22:00' && order.ordertime <'23:59' }">
																	<tr>
																		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
							                                        	<td><h6>RM11</h6></td>
						                                        	</tr>
						                                        	<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
						                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 0.06}"/></h6></td>
						                                    		</tr>
						                                    		<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
						                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 1.06 + 11}"/></h6></td>
						                                    		</tr>
																</c:when>
																<c:otherwise>
																	<tr>
																		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
							                                        	<td><h6>RM6</h6></td>
						                                        	</tr>
						                                        	<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
						                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 0.06}"/></h6></td>
						                                    		</tr>
						                                    		<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
						                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 1.06 + 6}"/></h6></td>
						                                    		</tr>
																</c:otherwise>
															</c:choose>
														</c:when>
												</c:choose>								  
										  </c:otherwise>
                                    </c:choose>
                     
                                    <tr>
                                        <th  class="font-weight-bold" colspan="3" style="text-align: left;">Payment Status</th>
                                        <th style="text-align: right;">${order.orderstatus}</th>
                                    </tr>
                                    <tr>
                                        <th  class="font-weight-bold" colspan="3" style="text-align: left;">Payment method</th>
                                        <th style="text-align: right;">${order.orderpayment}</th>
                                    </tr> 
                                                                       
                                </tbody>
                            </table>
                                                                           
                        <button class="btn btn-success btn-print mt-5"><i class="fas fa-print"></i> Print</button>
                    </div>
                </div>

            </div>
        </div>

    </div>
    </div>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        $('#payment_type').on('change', function (e) {
            var type = $(this).val();

            if(type == 'Cash'){
                $('#cash').attr('hidden', false);
                $('#card').attr('hidden', true);
            }else{
                $('#cash').attr('hidden', true);
                $('#card').attr('hidden', false);
            }
        });

        $('.btn-print').click(function(){
            // $('#print_receipt').print(); 
            
            var divToPrint = document.getElementById('print_receipt');

            var newWin = window.open('','Print-Window');

            newWin.document.open();

            newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

            newWin.document.close();

            setTimeout(function(){newWin.close();},10);

        });
    });
</script>

</html>