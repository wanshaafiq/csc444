<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<%
   session=request.getSession(false);
	csc444.model.userBean user = (csc444.model.userBean) session.getAttribute("sessionUser");
    if(user!=null){
    	if(user.getType().equalsIgnoreCase("admin")) {
        	response.sendRedirect("userServlet?action=menu_list");
	    } else if(user.getType().equalsIgnoreCase("customer")) {
	    	response.sendRedirect("userServlet?action=home");
	    }
    } 
%>  
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapid Rush Delivery</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"
        id="bootstrap-css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link href="CSS/userRegisterPage.css" rel="stylesheet">

</head>

<body>
    <div class="homepage-bg d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row mt-5">
                <div class="col-12 d-flex justify-content-center">
                    <div class="mb-5" id="formContent">
                        <h2>Customer Sign up</h2>
                        <div class="container text-left">
                            <form id="form_signup" action="userServlet?action=submit_signup" method="POST">
                                <div class="row">
                                    <div class="col-12">
                                        <label class="d-block" for="fullname">Full Name</label>
                                        <input type="text" id="fullname" class="fadeIn second" name="name">
                                        <small class="text-danger" id="error_fullname" hidden>Name cannot contain special character</small>
                                    </div>
                                </div>
                                <div class="row  mt-4">
                                    <div class="col-12">
                                        <label class="d-block" for="nric">NRIC</label>
                                        <input type="text" id="nric" class="fadeIn second" name="nric">
                                    </div>
                                </div>
                                 <div class="row  mt-4">
                                    <div class="col-12">
                                        <label class="d-block" for="address">Address</label>
                                        <input type="text" id="address" class="fadeIn second" name="address">
                                    </div>
                                </div>
                                <div class="row  mt-4">
                                    <div class="col-12">
                                        <label class="d-block" for="phone">Phone Number</label>
                                        <input type="text" id="phone" class="fadeIn second" name="phone">
                                    </div>
                                </div>
                                <div class="row  mt-4">
                                    <div class="col-12">
                                        <label class="d-block" for="username">Username</label>
                                        <input type="text" id="username" class="fadeIn second" name="username">
                                        <small class="text-danger" id="error_username" hidden>Username cannot contain space</small>
                                    </div>
                                </div>
                                <div class="row  mt-4">
                                    <div class="col-12">
                                        <label class="d-block" for="password">Password</label>
                                        <input type="password" id="password" class="fadeIn third" name="password">
                                    </div>
                                </div>
                                <div class="row  mt-4">
                                    <div class="col-12 text-center">
                                        <small class="text-danger" id="error_complete_form" style="display: block;" hidden>Please complete all the field before submit</small>
                                        <button class="btn btn-primary fadeIn fourth mt-5" id="signup">Sign Up</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 </div>
 
<script type="text/javascript">
            function regex_fullname(val) {
                var patt = "[^A-z\\s'][\\\^]?";
                var result = val.match(patt);

                if (result != null) {
                    $('#error_fullname').attr('hidden', false);
                    return true;
                } else {
                    $('#error_fullname').attr('hidden', true);
                    return false;
                }
            }

            function regex_username(val) {
                var patt = "[\\s][\\\^]?";
                var result = val.match(patt);
                if (result != null) {
                    $('#error_username').attr('hidden', false);
                    return true;
                } else {
                    $('#error_username').attr('hidden', true);
                    return false;
                }
            }

            $('#fullname, #phone, #username').on('change', function () {
                var fullname = $('#fullname').val();
                var phone = $('#phone').val();
                var username = $('#username').val();

                if (fullname != '')
                    regex_fullname(fullname);
                if (username != '')
                    regex_username(username);
            });

            $(document).ready(function () {
                $('#signup').on('click', function (event) {
                    event.preventDefault()
                    var fullname = $('#fullname').val();
                    var nric = $('#nric').val();
                    var address = $('#address').val();
                    var phone = $('#phone').val(); 
                    var username = $('#username').val();
                    var password = $('#password').val();
                    var no_error = false;

                    
                    $('#error_complete_form').attr('hidden',true)

                    if (fullname != '' && nric != '' && address !='' phone !='' && username != '' && password != '') {
                        no_error = true;
                        if (regex_fullname(fullname))
                            no_error = false;
                        if (regex_username(username))
                            no_error = false;
                    }else{
                        $('#error_complete_form').attr('hidden',false)
                    }
                    console.log(no_error)
                    if(no_error != false){
                        $('#form_signup').submit();
                    }
                });
            });
        </script>
                
</body>

</html>