<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapid Rush Delivery</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"
        id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" rel="stylesheet"
        id="fontawesome-css">
    <link href="CSS/cartPage.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="homepage-bg d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center my-4" style="color: #fff;">
                    <h1 class="font-weght-bold" style="text-shadow: 4px 4px rgb(31, 29, 29);">Rapid Rush Delivery</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <div id="formContent" style="width: 65vw; padding: 30px; margin-bottom: 50px;">
                        <form action="orderServlet?action=submit_order" method="POST">
                            <h3 class="font-weight-bold text-muted mb-4">Carts</h3>
                            <table class="table table-striped menu-food">
                                <thead>
                                    <tr>
                                        <th style="width: 45%; text-align: left;">Name</th>
                                        <th style="width: 10%;">Price</th>
                                        <th style="width: 25%;">Amount</th>
                                        <th style="width: 25%;">Subtotal</th>
                                    </tr>
                                </thead>
      
                                <tbody>
                                	<c:forEach items="${OrderList}" var="menu" varStatus="loop">
                                	<c:if test="${OrderList[loop.index].menuqty > 0}">
                                    <tr>
                                        <td style="text-align: left;">${menu.menuname}</td>
                                        <td>${menu.menuprice}</td>
                                        <td>${menu.menuqty}</td>
                                        <td>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${menu.menuprice * OrderList[loop.index].menuqty}"/></td>
                                    </tr>
                                    </c:if>
                                    </c:forEach>						
									<c:choose>
										<c:when test="${promo == 'STAYATHOME10' }">
											<c:choose>
												<c:when test="${total >15 }">
													<c:choose>
															<c:when test="${time >'22:00' && time <'23:59' }">
																<tr>
																	<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
						                                        	<td><h6>RM5</h6></td>
					                                        	</tr>
					                                        	<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
					                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 0.06}"/></h6></td>
					                                    		</tr>
					                                    		<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
					                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 1.06 + 5}"/></h6></td>
					                                    		</tr>
															</c:when>
															<c:otherwise>
																<tr>
																	<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
						                                        	<td><h6>Free of charge</h6></td>
					                                        	</tr>
					                                        	<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
					                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 0.06}"/></h6></td>
					                                    		</tr>
					                                    		<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
					                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 1.06 }"/></h6></td>
					                                    		</tr>
															</c:otherwise>
														</c:choose>
												  </c:when>
												  <c:when test="${total <15 }">
												  		<c:choose>
																<c:when test="${time >'22:00' && time <'23:59' }">
																	<tr>
																		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
							                                        	<td><h6>RM11</h6></td>
						                                        	</tr>
						                                        	<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
						                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 0.06}"/></h6></td>
						                                    		</tr>
						                                    		<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
						                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 1.06 + 11}"/></h6></td>
						                                    		</tr>
																</c:when>
																<c:otherwise>
																	<tr>
																		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
							                                        	<td><h6>RM6</h6></td>
						                                        	</tr>
						                                        	<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
						                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 0.06}"/></h6></td>
						                                    		</tr>
						                                    		<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
						                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total *0.9 * 1.06 + 6}"/></h6></td>
						                                    		</tr>
																</c:otherwise>
														</c:choose>
													</c:when>
											  </c:choose>	  		
										  </c:when>
										  <c:otherwise>
										  		<c:choose>
												  	<c:when test="${total >15 }">
												  		<c:choose>
															<c:when test="${time >'22:00' && time <'23:59'}">
																<tr>
																	<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
						                                        	<td><h6>RM5</h6></td>
					                                        	</tr>
					                                        	<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
					                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 0.06}"/></h6></td>
					                                    		</tr>
					                                    		<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
					                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 1.06 + 5}"/></h6></td>
					                                    		</tr>
															</c:when>
															<c:otherwise>
																<tr>
																	<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
						                                        	<td><h6>Free of charge</h6></td>
					                                        	</tr>
					                                        	<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
					                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 0.06}"/></h6></td>
					                                    		</tr>
					                                    		<tr>
					                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
					                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 1.06 }"/></h6></td>
					                                    		</tr>
															</c:otherwise>
														 </c:choose>
													  </c:when>		  
													  <c:when test="${total <15 }">
													  		<c:choose>
																<c:when test="${time >'22:00' && time <'23:59' }">
																	<tr>
																		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
							                                        	<td><h6>RM11</h6></td>
						                                        	</tr>
						                                        	<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
						                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 0.06}"/></h6></td>
						                                    		</tr>
						                                    		<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
						                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 1.06 + 11}"/></h6></td>
						                                    		</tr>
																</c:when>
																<c:otherwise>
																	<tr>
																		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Delivery Cost</h6></td>
							                                        	<td><h6>RM6</h6></td>
						                                        	</tr>
						                                        	<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">6% SST</h6></td>
						                                        		<td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 0.06}"/></h6></td>
						                                    		</tr>
						                                    		<tr>
						                                        		<td colspan="3" style="text-align: left;"><h6 class="font-weight-bold">Total Price</h6></td>
						                                       			 <td><h6>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${total * 1.06 + 6}"/></h6></td>
						                                    		</tr>
																</c:otherwise>
															</c:choose>
														</c:when>
												</c:choose>								  
										  </c:otherwise>
                                    </c:choose>                                    
                                </tbody>
                            </table>
                            <div class="row mt-5">
                                <div class="col-12 text-left">
                                    <label for="payment_type" class="d-block">Payment Type</label>
                                    <select name="payment_type" id="payment_type">
                                        <option value="0" selected disabled>Select Type</option>
                                        <option value="Card">Card</option>
                                        <option value="Cash">Cash</option>
                                    </select>
                                </div>
                            </div>

                            <div id="card" hidden>
                                <div class="row mt-3">
                                    <div class="col-12 text-left">
                                        <label for="card_number" class="d-block">Card Number</label>
                                        <input type="text" name="card_number" id="card_number">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-6 text-left">
                                        <label for="card_cvv" class="d-block">CVV</label>
                                        <input type="text" name="card_cvv" id="card_cvv">
                                    </div>
                                    <div class="col-6 text-left">
                                        <label for="card_expire" class="d-block">Expire</label>
                                        <input type="text" name="card_expire" id="card_expire">
                                    </div>
                                </div>
                            </div>

                            <div id="cash" hidden>
                                <h5 class="font-weight-bold mt-5">Please pay at delivery worker</h5>
                            </div>
                            
                            <input type="submit" value="Submit" class="btn btn-primary mt-5">
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        $('#payment_type').on('change', function (e) {
            var type = $(this).val();

            if(type == 'Cash'){
                $('#cash').attr('hidden', false);
                $('#card').attr('hidden', true);
            }else{
                $('#cash').attr('hidden', true);
                $('#card').attr('hidden', false);
            }
        });
    });
</script>

</html>