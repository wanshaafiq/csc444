<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Rapid Rush Delivery</title>

 <style>
 .button {
  background-color: #254932 ;
  color: white;
  padding: 5px 5px;
  margin-left:30%; 
  margin-right:30%;


}
 table {
 
  border-spacing: 1;
  border-collapse: collapse;
  background: #A2BBAB;
  border-radius: 6px;
  overflow: hidden;
  max-width: 500px;
  width:25%; 
  margin-left:40%; 
  margin-right:20%;

}
table * {
  width:25%;
  
}
table td, table th {
  padding-left: 5px;
  height: 70px;
}
table thead tr {
  height: 40px;
  background: #254932;
  font-size: 16px;
  color: white;
  text-align: center;
}

body {
 
  font: 400 14px 'Calibri','Arial';
  padding: 20px;
  background-image: url('https://www.gloriafood.com/wp-content/uploads/2021/03/How_to_Improve_Your_Food_Delivery_Service_in_2022_-_fb.png');
  min-height: 100%;
  background-size: cover;
}

</style>

</head>
<body>
	
	<form id="update_price" name="update_price" method="post" action="menuServlet?action=update_price">
	<input type="hidden" name="menuid" value="${menu.menuid}">
	<br><br><br><br><br><br><br><br><br>
	
	<input type="hidden" name="menuname" value="${menu.menuname}">
	<table>
		<thead>
			<tr>
				<th colspan='2'><font size="6" ><b>${menu.menuname}</b></font></th>
			</tr>
		<thead>
		<tbody>

			<tr align="center">
				<td colspan="2"><font size="4" face="Georgia"><b>Price:<b><font size="3" face="Georgia"> RM <input type="text" name="menuprice" value="${menu.menuprice}"></font></font></td>
			</tr>
			
			<tr>
				
					<td><a href="menuServlet?action=menu_list&menuid=${menu.menuid}"><button class="button">Back</button></a>
					<td><a href="menuServlet?action=update_price&menuid=${menu.menuid}"><button class="button">Update</button></a>
			</tr>

		</tbody>
	</table>
</form>
</body>
</html>