<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapid Rush Delivery</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"
        id="bootstrap-css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link href="CSS/Login.css" rel="stylesheet">
	    
</head>
<body>
    <div class="homepage-bg d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center my-4" style="color: #A975BF;">
                    <h1>Rapid Rush Delivery</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <div id="formContent">
                        <ul class="nav nav-tabs">
                            <li class="active"><a class="px-auto active" data-toggle="tab" href="#customer">Customer</a></li>
                            <li><a class="px-auto" data-toggle="tab" href="#admin">Admin</a></li>
                        </ul>
    
                        <div class="tab-content">
                            <div id="customer" class="tab-pane fade in show active">
                            	<h2 class="font-weight mb-5">Customer</h2>
                                <form action="userServlet?action=submit_login" method="POST">
                                    <input type="text" id="username" class="fadeIn second" name="username" placeholder="Username">
                                    <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
                                    <input type="text" name="type" value="Customer" hidden>
                                    <input type="submit" class="fadeIn fourth" value="Log In">
                                </form>
                                <h6 class="mb-5">Don't have account? <a href="userServlet?action=signup">Signup</a></h6>
                            </div>
                            <div id="admin" class="tab-pane fade">
                                <h2 class="font-weight mb-5">Admin</h2>
                                <form action="userServlet?action=submit_login" method="POST">
                                    <input type="text" id="username" class="fadeIn second" name="username" placeholder="Username">
                                    <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
                                    <input type="text" name="type" value="Admin" hidden>
                                    <input type="submit" class="fadeIn fourth" value="Log In">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
</body>

<script type="text/javascript">
    $(document).ready(function(){
        $('.nav-tabs li a').on('click', function(){
            $('.nav-tabs li').removeClass('active');
            $(this).parent().addClass('active');
        })
    });
</script>

</html>