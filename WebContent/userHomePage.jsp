<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapid Rush Delivery</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" rel="stylesheet" id="fontawesome-css">
    <link href="CSS/userHomePage.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<style>
input[type=time] {
  width: 35%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
}
input[type=text] {
  width: 40%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
}
</style>

</head>

<body>
        <div class="homepage-bg d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center my-4" style="color: #fff;">
                    <h1 class="font-weght-bold" style="text-shadow: 4px 4px rgb(31, 29, 29);">Rapid Rush Delivery</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <div id="formContent" style="width: 65vw; padding: 30px; margin-bottom: 50px;">
                    <div class="row mb-4">
	                    <div class="col-12 text-right">
                           	<a href="userServlet?action=logout" class="btn btn-primary">Logout <i class="fas fa-sign-out-alt"></i></a>
	                    </div>
                    </div>
                      <form action="orderServlet?action=create_order" method="POST">
                      	<input type="number" name="order_id" value="${orderid}" hidden>
                        <h3>KFC</h3>
                        <table class="table table-striped menu-food">
                          <thead>
                            <tr>
                              <th style="width: 45%; text-align: left;">Menu</th>
                              <th style="width: 20%;">Price</th>
                              <th style="width: 25%;">Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                          	<c:forEach items="${menuList}" var="menu">
                          	<c:if test="${menu.menutype == 'KFC'}">
	                          		<tr>
		                              <td style="text-align: left;">${menu.menuname}</td>
		                              <td>RM<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${menu.menuprice}"/></td>
		                              <td>
		                                <button class="btn border-0 d-inline minus" data-menuid="${menu.menuid}" style="color: red;"><i class="fas fa-minus-circle"></i></button>
		                               
										<input type="number" id="qty-${menu.menuid}" name="qty-${menu.menuid}" class="amount w-25 d-inline" value="0" style="text-align: center;" readonly>

		                                <button class="btn border-0  d-inline plus" data-menuid="${menu.menuid}" style="color: green;"><i class="fas fa-plus-circle"></i></button>
		                              </td>
		                            </tr>
		                       </c:if>
                          	</c:forEach>
                          </tbody>
                        </table>

                        <h3>Donut & Coffee</h3>
                        <table class="table table-striped menu-food">
                          <thead>
                            <tr>
                              <th style="width: 45%; text-align: left;">Menu</th>
                              <th style="width: 20%;">Price</th>
                              <th style="width: 25%;">Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                            <c:forEach items="${menuList}" var="menu">
                          		<c:if test="${menu.menutype == 'Donut'}">
	                          		<tr>
		                              <td style="text-align: left;">${menu.menuname}</td>
		                              <td>RM<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  value="${menu.menuprice}"/></td>
		                              <td>
		                              	
		                                <button class="btn border-0 d-inline minus" data-menuid="${menu.menuid}" style="color: red;"><i class="fas fa-minus-circle"></i></button>
		                                
										<input type="number" id="qty-${menu.menuid}" name="qty-${menu.menuid}" class="amount w-25 d-inline" value="0" style="text-align: center;" readonly>
										
		                                <button class="btn border-0  d-inline plus" data-menuid="${menu.menuid}" style="color: green;"><i class="fas fa-plus-circle"></i></button>
		                              </td>
		                            </tr>
                          		</c:if>
                          	</c:forEach>
                          </tbody>
                        </table>
                        
                        <h3>Delivery Time</h3>
                         <table class="table table-striped menu-food">
                          <thead>
                            <tr>
                              <th align="center">Time</th>
                            </tr>
                          </thead>
                           <tbody>
                               <tr>
                                   <td><input type="time" id="time" name="time"/>
                               </tr>
                           </tbody>
                          </table>

                         <h3>Promo Code</h3>
                         <table class="table table-striped menu-food">
                          <thead>
                            <tr>
                              <th align="center">Code</th>
                            </tr>
                          </thead>
                           <tbody>
                               <tr>
                                   <td><input type="text" id="promo" name="promo" value="" />
                               </tr>
                           </tbody>
                          </table>
                          
                       <input type="submit" value="Proceed to Payment" class="btn btn-primary mt-5">
                      </form>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
</body>

<script type="text/javascript">
    $(document).ready(function(){
        $('.minus').on('click', function(e){
            e.preventDefault();
            var id = $(this).data('menuid');
            var amount = parseInt($('#qty-' + id).val());
            
            if(amount > 0)
              $('#qty-' + id).val(amount - 1);
        });

        
        $('.plus').on('click', function(e){
          e.preventDefault();
          var id = $(this).data('menuid');
          var amount = parseInt($('#qty-' + id).val());
          
          $('#qty-' + id).val(amount + 1);
        });
    });
</script>

</html>