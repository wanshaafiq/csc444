package csc444.model;

public class orderhistoryBean {

	private int userid;
	private int orderid;
	private String ordertime;
	private String orderstatus;
	
	public orderhistoryBean() {
		
		userid = 0;
		orderid = 0;
		ordertime ="";
		orderstatus = "";
	}
	
	public orderhistoryBean(int userid, int orderid, String ordertime, String orderstatus) {
		super();
		this.userid = userid;
		this.orderid = orderid;
		this.ordertime = ordertime;
		this.orderstatus = orderstatus;
	}
	
	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public String getOrdertime() {
		return ordertime;
	}

	public void setOrdertime(String ordertime) {
		this.ordertime = ordertime;
	}

	public String getOrderstatus() {
		return orderstatus;
	}

	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	
}
