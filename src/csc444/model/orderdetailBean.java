package csc444.model;

public class orderdetailBean {

	private int orderid;
	private int menuid;
	private int menuqty;
	private boolean valid;
	
	public orderdetailBean() {
		this.orderid = 0;
		this.menuid = 0;
		this.menuqty = 0;
		this.valid = false;
	}
	
	public orderdetailBean(int orderid, int menuid, int menuqty, boolean valid) {
		super();
		this.orderid = orderid;
		this.menuid = menuid;
		this.menuqty = menuqty;
		this.valid = valid;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public int getMenuid() {
		return menuid;
	}

	public void setMenuid(int menuid) {
		this.menuid = menuid;
	}

	public int getMenuqty() {
		return menuqty;
	}

	public void setMenuqty(int menuqty) {
		this.menuqty = menuqty;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	@Override
	public String toString() {
		return "orderid=" + orderid + ", menuid=" + menuid + ", menuqty=" + menuqty;
	}
}
