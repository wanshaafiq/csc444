package csc444.model;

public class menuBean {
	
	private int menuid;
	private String menuname;
	private double menuprice;
	private String menutype;
	private boolean valid;

	public menuBean() {
		
		this.menuid = 0;
		this.menuname = "";
		this.menuprice = 0.00;
		this.menutype = "";
		this.valid = false;
		
	}

	public menuBean(int menuid, String menuname, double menuprice, String menutype, boolean valid) {
		
		this.menuid = menuid;
		this.menuname = menuname;
		this.menuprice = menuprice;
		this.menutype = menutype;
		this.valid = valid;

	}
	
	public int getMenuid() {
		return menuid;
	}

	public void setMenuid(int menuid) {
		this.menuid = menuid;
	}

	public String getMenuname() {
		return menuname;
	}

	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}

	public double getMenuprice() {
		return menuprice;
	}

	public void setMenuprice(double menuprice) {
		this.menuprice = menuprice;
	}

	public String getMenutype() {
		return menutype;
	}

	public void setMenutype(String menutype) {
		this.menutype = menutype;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}


	@Override
	public String toString() {
		return "menuid = " + menuid + ", menuname = " + menuname + ", menuprice = " + menuprice + ", menutype = "
				+ menutype;
	}

}
