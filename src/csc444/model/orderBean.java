package csc444.model;

public class orderBean {

	private int orderid;
	private int userId;
	private String ordertime;
	private String orderstatus;
	private String orderpayment;
	private String orderpromo;
	private boolean valid;

	public orderBean() {
		
		this.orderid = 0;
		this.userId = 0;
		this.ordertime = "";
		this.orderstatus = "";
		this.orderpayment = "";
		this.orderpromo = "";
		this.valid = false;
		
	}

	public orderBean(int orderid, int userId, String ordertime, String orderstatus, String orderpayment,String orderpromo
			,boolean valid) {
		super();
		this.orderid = orderid;
		this.userId = userId;
		this.ordertime = ordertime;
		this.orderstatus = orderstatus;
		this.orderpayment = orderpayment;
		this.orderpromo = orderpromo;
		this.valid = valid;
	}
	
	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getOrdertime() {
		return ordertime;
	}

	public void setOrdertime(String ordertime) {
		this.ordertime = ordertime;
	}

	public String getOrderstatus() {
		return orderstatus;
	}

	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}

	public String getOrderpayment() {
		return orderpayment;
	}

	public void setOrderpayment(String orderpayment) {
		this.orderpayment = orderpayment;
	}
	
	public String getOrderpromo() {
		return orderpromo;
	}

	public void setOrderpromo(String orderpromo) {
		this.orderpromo = orderpromo;
	}


	public boolean isValid() {
		return this.valid; 
	}
	
	public void setValid(boolean valid) { 
		this.valid = valid; 
	}
	

	@Override
	public String toString() {
		return "orderid=" + orderid + ", userId=" + userId + ", ordertime=" + ordertime + ", orderstatus="
				+ orderstatus + ", orderpayment=" + orderpayment + ", valid=" + valid;
	}
	
}
