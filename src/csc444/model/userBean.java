package csc444.model;

public class userBean {

	private int userid;
	private String name;
	private String nric;
	private String username;
	private String password;
	private String type;
	private String address;
	private String phonenumber;
	private boolean valid;

	public userBean() {
		
		this.userid = 0;
		this.name = "";
		this.nric = "";
		this.username = "";
		this.password = "";
		this.type = "";
		this.address = "";
		this.phonenumber = "";
		this.valid = false;
		
	}

	public userBean(int userid, String name, String nric, String username, String password, String type,String address,String phonenumber, boolean valid) {
		
		this.userid = userid;
		this.name = name;
		this.nric = nric;
		this.username = username;
		this.password = password;
		this.type = type;
		this.address = address;
		this.phonenumber = phonenumber;
		this.valid = valid;

	}
	
	public int getUserid() {
		return this.userid; 
	}
	
	public void setUserid(int userid) { 
		this.userid = userid; 
	}
	
	public String getName() { 
		return this.name; 
	}
	
	public void setName(String name) {
		this.name = name; 
	}

	public String getNric() {
		return this.nric;
	}
	
	public void setNric(String nric) { 
		this.nric = nric; 
	}

	public String getUsername() {
		return this.username; 
	}
	
	public void setUsername(String username) { 
		this.username = username; 
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getType() { 
		return type; 
	}
	
	public void setType(String type) {
		this.type = type; 
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}


	public boolean isValid() { 
		return this.valid; 
	}
	
	public void setValid(boolean valid) { 
		this.valid = valid; 
	}
		
	public String toString() {
		String text = "";
		
		text += "User ID: " + this.userid;
		text += ", Name: " + this.name;
		
		return text;
	}
	
}
