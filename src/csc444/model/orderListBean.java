package csc444.model;

public class orderListBean {
	
	private int menuid;
	private String menuname;
	private double menuprice;
	private int menuqty;
	private double total;
	
	
	public orderListBean() {
		
		menuid = 0;
		menuname = "";
		menuprice = 0.00;
		menuqty = 0;
		total=0.00;
		
		
	}
	
	public orderListBean(int menuid, String menuname, double menuprice, int menuqty,double total) {
		
		this.menuid = menuid;
		this.menuname = menuname;
		this.menuprice = menuprice;
		this.menuqty = menuqty;
		this.total=total;

	}

	public int getMenuid() {
		return menuid;
	}

	public void setMenuid(int menuid) {
		this.menuid = menuid;
	}

	public String getMenuname() {
		return menuname;
	}

	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}

	public double getMenuprice() {
		return menuprice;
	}

	public void setMenuprice(double menuprice) {
		this.menuprice = menuprice;
	}

	public int getMenuqty() {
		return menuqty;
	}

	public void setMenuqty(int menuqty) {
		this.menuqty = menuqty;
	}
	
	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "menuid = " + menuid + ", menuname = " + menuname + ", menuprice = " + menuprice + ", menuqty = "
				+ menuqty + " total" + total;
	}

}
