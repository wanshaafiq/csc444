package csc444.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import csc444.dao.OrderDAO;
import csc444.dao.UserDAO;
import csc444.model.orderhistoryBean;
import csc444.model.userBean;


/**
 * Servlet implementation class userServlet
 */
@WebServlet("/userServlet")
public class userServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UserDAO userDAO;
      
	String forward = "";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public userServlet() {
        super();
        userDAO = new UserDAO();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		
			
			if(action.equalsIgnoreCase("loginpage")) {
				
				//forward to jsp
				forward = "LoginPage.jsp";
				
				RequestDispatcher view = request.getRequestDispatcher(forward);
				view.forward(request, response);
				
			} else if(action.equalsIgnoreCase("signup")) {	
				
				//forward to jsp
				forward = "userRegisterPage.jsp";
				
				RequestDispatcher view = request.getRequestDispatcher(forward);
				view.forward(request, response);
				
			} else if(action.equalsIgnoreCase("logout")) {
				//session end
				session.invalidate();

				System.out.println("Successfully logout");
				
				//forward to jsp
				forward = "LoginPage.jsp";
				
				RequestDispatcher view = request.getRequestDispatcher(forward);
				view.forward(request, response);
				
			} else if (action.equalsIgnoreCase("user_orderpending")) {
				orderPending(request,response);
				
			} else if (action.equalsIgnoreCase("update_pendingstatus")) {
				updatePending(request,response);
				
			} else if(action.equalsIgnoreCase("user_orderhistory")) {
				viewPaid(request,response);
				
			}
			
	}

	//admin view order history
	private void viewPaid(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Display list of paid order
		request.setAttribute("paidorder", OrderDAO.listpaidOrder());
		
		//forward to jsp
		forward ="customerOrderHistory.jsp";
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response); 
	}

	//admin update pending customer order
	private void updatePending(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//get order id from from jsp
		int orderid = Integer.parseInt(request.getParameter("orderid"));
		
		//update pending order by order id
		OrderDAO.updatependingstatus(orderid);
		
		//forward to user pending action
        request.setAttribute("redirect_path", "userServlet?action=user_orderpending");
        forward = "redirect.jsp";
        
        RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response); 
		
	}

	private void orderPending(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Display list of pending order
		request.setAttribute("order", OrderDAO.custlistpendingOrder());
		
		//forward to jsp
		forward ="viewCustOrder.jsp";
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response); 
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		String action = request.getParameter("action");
		
		if(action.equalsIgnoreCase("submit_login")) {
			
			try {
				loginUser(request,response);
				
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		
		else if (action.equalsIgnoreCase("submit_signup")) {		
						
			try {
				signupUser(request,response);
				
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
				
		}
	}

	private void signupUser(HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException, ServletException, IOException {
		HttpSession session = request.getSession();
		
		//get data from jsp
		String name = request.getParameter("name");
		String nric = request.getParameter("nric");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String address = request.getParameter("address");
		String phonenumber = request.getParameter("phone");
		String type = "Customer";
		
		//check registered user
		userBean user = userDAO.checkUser(username);
		
		//check of user data exist
		if (user.isValid()==true) {
			System.out.println("User already register");
			
			//forward to signup action
			request.setAttribute("redirect_path", "userServlet?action=signup");
			forward = "redirect.jsp";
			
		}
		if(user.isValid()==false) {	
			//initialize user session		
			session.setAttribute("sessionUser", user);
			
			//register user to DB
			userDAO.registerUser(name, nric, username, password, type,address,phonenumber);
			
			//forward to user home action
			request.setAttribute("redirect_path", "menuServlet?action=home");
			forward = "redirect.jsp";
		}
			
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
		
	}

	private void loginUser(HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException, ServletException, IOException {
		HttpSession session = request.getSession();
		
		//get data from jsp
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String type = request.getParameter("type");
		
		//check user exist
		userBean user = userDAO.UserLogin(username, password, type);
		
		//check registered user
		if(user.isValid() == true) {
			
			//initialize session
			session.setAttribute("sessionUser", user);

			//check user type
			if(user.getType().equalsIgnoreCase("admin")) {
				
				//forward to menu list action
				request.setAttribute("redirect_path", "menuServlet?action=menu_list");
			}else {
				
				//forward to user home action
				request.setAttribute("redirect_path", "menuServlet?action=home");
			}			
			forward = "redirect.jsp";
			
		} else {
			
			//forward to login page
			request.setAttribute("redirect_path", "userServlet?action=loginpage");
			forward = "redirect.jsp";
		}
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
		
	}
	
	

}
