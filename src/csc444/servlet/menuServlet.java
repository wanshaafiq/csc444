package csc444.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import csc444.dao.MenuDAO;
//import csc444.dao.OrderDAO;
//import csc444.dao.OrderdetailDAO;
import csc444.model.menuBean;
import csc444.model.orderBean;
import csc444.model.orderdetailBean;
import csc444.model.userBean;

/**
 * Servlet implementation class menuServlet
 */
@WebServlet("/menuServlet")
public class menuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private MenuDAO menuDAO;

	String forward = "";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public menuServlet() {
		super();
		menuDAO = new MenuDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("home")) {
			homePage(request, response);
			
		} else if (action.equalsIgnoreCase("menu_list")) {
			adminHomepage(request, response);
		}
		if (action.equalsIgnoreCase("edit")) {
			adminEdit(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("update_price")) {
			adminUpdatePrice(request, response);
		}

	}

	//edit menu price
	private void adminEdit(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			
			//get menu id from from jsp
			int menuid = Integer.parseInt(request.getParameter("menuid"));

			//get menu detail by menu id
			menuBean menu = menuDAO.getMenubyid(menuid);

			//set attribute
			request.setAttribute("menu", menu);
			
			//forward to jsp
			forward = "updatePrice.jsp";

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	// update menu price
	private void adminUpdatePrice(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//get data from from jsp
		int menuid = Integer.parseInt(request.getParameter("menuid"));
		String menuname = request.getParameter("menuname");
		double menuprice = Double.parseDouble(request.getParameter("menuprice"));

		//update menu price 
		menuDAO.updateMenuPrice(menuid, menuname, menuprice);

		//forward to menu list action.
		request.setAttribute("redirect_path", "menuServlet?action=menu_list");
		forward = "redirect.jsp";

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	//user home page
	private void homePage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			// Get list of all menus
			ArrayList<menuBean> menuList = menuDAO.getAllMenu();
			//set attribute
			request.setAttribute("menuList", menuList);

			//forward to jsp
			forward = "userHomePage.jsp";

		} catch (Throwable ex) {
			ex.printStackTrace();
		}

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	//admin home page
	private void adminHomepage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			//display menu list
			ArrayList<menuBean> menuList = menuDAO.getAllMenu();

			//set attribute
			request.setAttribute("menuList", menuList);
			
			//forward to jsp
			forward = "addMenuPage.jsp";

		} catch (Exception e) {
			e.printStackTrace();
		}

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}
}
