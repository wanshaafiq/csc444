package csc444.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import csc444.dao.MenuDAO;
import csc444.dao.OrderdetailDAO;
import csc444.dao.OrderDAO;
import csc444.model.menuBean;
import csc444.model.orderBean;
import csc444.model.orderListBean;
import csc444.model.orderdetailBean;
import csc444.model.userBean;

/**
 * Servlet implementation class orderServlet
 */
@WebServlet("/orderServlet")
public class orderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private MenuDAO menuDao;
	private OrderdetailDAO orderdetailDao;
		
	String forward = "";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public orderServlet() {
		super();
		menuDao = new MenuDAO();
		orderdetailDao = new OrderdetailDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("create_order")) {
			createOrder(request, response);
		}
		else if (action.equalsIgnoreCase("submit_order")) {
			submitOrder(request,response);
		}
	}

	//customer submit order
	private void submitOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//get user session 
		userBean user = (userBean) session.getAttribute("sessionUser");
		
		//get payment type from jsp
		String paymenttype = request.getParameter("payment_type");
		
		//get user id from session
		int userid = user.getUserid();
		
		//check user order
		int orderid = OrderDAO.checkorder(userid);
			
		//check payment type 
		if(paymenttype.equalsIgnoreCase("Card")) {
			//update order payment
			OrderDAO.updateStatusCard(userid,orderid);			
		}
		else if(paymenttype.equalsIgnoreCase("Cash")) {
			//update order payment 
			OrderDAO.updateStatusCash(userid,orderid);
		}

		//get order status,time and promo code 
		orderBean order = OrderDAO.getOrderStatus(userid,orderid);
		request.setAttribute("order",order);
		
		//get user name and address
		userBean users = OrderDAO.getUserdetail(userid);
		request.setAttribute("users",users);
		
		//get order list
		ArrayList<orderListBean> OrderList = OrderdetailDAO.getAllOrderListReceipt(orderid);
		double total = OrderdetailDAO.TotalPrice(OrderList);
		
		//set attribute
		request.setAttribute("OrderList",OrderList);
		request.setAttribute("total",total);
		
		//forward to jsp
		forward="Receipt.jsp";
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response); 
	}

	//customer create order
	private void createOrder(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		//get user session 
		userBean user = (userBean) session.getAttribute("sessionUser");
		
		//get time and promo code from jsp
		String time = request.getParameter("time");
		String promocode = request.getParameter("promo");
				
		//set attribute for time and promo code
		request.setAttribute("promo",promocode);
		request.setAttribute("time",time);
			
		//get user id from session
		int userid = user.getUserid();
		
		//check user order
		int orderid = OrderDAO.checkorder(userid);
		
		orderBean order = new orderBean();

		//check if user have order id or not
		if (orderid == 0) {
			//create order for user
			order = OrderDAO.createOrder(userid);

			//get user orderid
			orderid = order.getOrderid();

		}
		
		//insert time and promo code to database
		OrderDAO.insertTimePromo(time,user.getUserid(),orderid,promocode);
				
		//display menu list 
		 ArrayList<menuBean> menuList = menuDao.getAllMenu();
		 
		 // get all menu user choose to add to order detail table 
		 for (int i = 0; i <menuList.size(); i++) { 
			 
			 //get menu id from menu list
			 int menuId = menuList.get(i).getMenuid();
			 //get quantity base on menuid
			 int qty = Integer.parseInt(request.getParameter("qty-" + menuId));
		  
		// check menu qty selected by user
		  if (qty > 0) { 
			  
		  orderdetailBean orderMenu = new orderdetailBean();
		  //set orderid and menu qty
		  orderMenu.setOrderid(orderid); orderMenu.setMenuid(menuId);
		  orderMenu.setMenuqty(qty);
		  
		  //add menu selected to order detail table
		  OrderdetailDAO.createorderDetail(orderMenu); } 
		  }
		
		 //display order list by order id
		ArrayList<orderListBean> OrderList = OrderdetailDAO.getAllOrderList(orderid);
		
		//calculate total price base on order list from user order detail table
		double total = OrderdetailDAO.TotalPrice(OrderList);
		
		//set attribute
		request.setAttribute("OrderList",OrderList);
		request.setAttribute("total",total);
		
		//forward to jsp
		forward="cartPage.jsp";
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response); 

	}

}
