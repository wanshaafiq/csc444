package csc444.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import csc444.connection.ConnectionManager;
import csc444.model.userBean;

import java.security.NoSuchAlgorithmException;

public class UserDAO {

	static Connection conn = null;
	static ResultSet rs = null;
	static PreparedStatement ps = null;
	static Statement stmt = null;

	public UserDAO() {
		conn = ConnectionManager.getConnection();
	}
	
	public userBean UserLogin(String username, String password, String type) throws NoSuchAlgorithmException {
		userBean user = new userBean();
		
        try {
        	
        	String query = "SELECT * from users where username=? and password=? and type=?";
			
        	ps = conn.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, type);
            rs = ps.executeQuery();

	        if(conn != null) {
	            
	            // if user exists return true
	            if (rs.next()) {	
	            	
	           		System.out.println("Welcome, " + rs.getString("username"));
	           		
	           		user.setUserid(rs.getInt("userid"));
	                user.setName(rs.getString("name"));
	                user.setNric(rs.getString("nric"));
	                user.setUsername(rs.getString("username"));
	                user.setType(rs.getString("type"));
	                user.setAddress(rs.getString("address"));
	                user.setPhonenumber(rs.getString("phonenumber"));
	                user.setValid(true);
	                
	           	} else {       		
	            	System.out.println(" Invalid user! Please register first");
	            	
	            	// if user does not exist set the isValid variable to false
	            	user.setValid(false);	            	
	            }	            
	            
	        } else {	        	
	        	System.out.println("Connection is null");
	        	
	        }
	        
        } catch (SQLException ex) {
        	
            System.out.println("User Log In failed: An Exception has occurred! " + ex);
            ex.printStackTrace();
            
        } finally {
        	
            try { 
            	
            	ps.close(); 
            	rs.close();  
            	
            } catch (Exception ignore) { }
            
        }
        
		return user;
   
    }
	
	public userBean registerUser(String name, String nric, String username, String password, String type,String address ,String phonenumber) throws NoSuchAlgorithmException {
		
		userBean user = new userBean();
        try {
        	
        	String query = "insert into users(userid, name, nric, username, password, type,address,phonenumber) values (USER_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?)";
        	String generatedColumns[] = { "userid" };
        	
        	ps = conn.prepareStatement(query, generatedColumns);
        	
        	ps.setString(1, name);
            ps.setString(2, nric);
            ps.setString(3, username);
            ps.setString(4, password);
            ps.setString(5, type);
            ps.setString(6, address);
            ps.setString(7, phonenumber);
            ps.executeUpdate();
            

            int generateId = 0;
            
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                	generateId = rs.getInt(1);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
               		
            user = new userBean(generateId, name, nric, username, password, type,address,phonenumber, true);
	        
        } catch (SQLException ex) {
        	
            System.out.println("register failed: An Exception has occurred! " + ex);
            ex.printStackTrace();
            
        } finally {
        	
            try { 
            	
            	ps.close(); 
            	rs.close();  
            	
            } catch (Exception ignore) { }
            
        }
        
		return user;
   
    }

	public userBean checkUser(String username) {
		userBean user = new userBean();

		 String searchQuery = "select * from users where username='" + username + "'";

       try {
           conn = ConnectionManager.getConnection();
           stmt = conn.createStatement();
           rs = stmt.executeQuery(searchQuery);
           boolean more = rs.next();
    
           
        // if register data exists set the isValid variable to true
           if (more) {
        	   user.setValid(true);
          	}
           else if (!more) {      
        	   user.setValid(false);
           } 
       }

       catch (Exception ex) {
           System.out.println("check user failed: An Exception has occurred! " + ex);
       }

       finally {
       	
           try { 
           	
           	ps.close(); 
           	rs.close();  
           	
           } catch (Exception ignore) { }
           
       }

       return user;
	}
	
	
}

