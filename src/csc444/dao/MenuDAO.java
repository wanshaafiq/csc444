package csc444.dao;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import csc444.connection.ConnectionManager;
import csc444.model.menuBean;

public class MenuDAO {
	
	static Connection conn = null;
	static ResultSet rs = null;
	static PreparedStatement ps = null;
	static Statement stmt = null;

	public MenuDAO() {
		conn = ConnectionManager.getConnection();
	}
	
	public ArrayList<menuBean> getAllMenu() {
		ArrayList<menuBean> menuList = new ArrayList<menuBean>();

		try {

			String query = "SELECT * FROM MENU";

			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();

			if (conn != null) {

				// if user exists return true
				while (rs.next()) {

					menuBean menu = new menuBean();

					menu.setMenuid(rs.getInt("menuid"));
					menu.setMenuname(rs.getString("menuname"));
					menu.setMenuprice(rs.getDouble("menuprice"));
					menu.setMenutype(rs.getString("menutype"));
					menu.setValid(true);
					menuList.add(menu);
				}

			} else {

				System.out.println("Connection is null");

			}

		} catch (SQLException ex) {

			System.out.println("get all menu failed: An Exception has occurred! " + ex);
			ex.printStackTrace();

		} finally {

			try {

				ps.close();
				rs.close();

			} catch (Exception ignore) {
			}

		}

		return menuList;

	}
	
	// getmenu by menuid
		public menuBean getMenubyid(int menuid) {

			menuBean menu = new menuBean();

			try {
				conn = ConnectionManager.getConnection();
				ps = conn.prepareStatement("select * from menu where menuid=?");

				ps.setInt(1, menuid);
				ResultSet rs = ps.executeQuery();


				if (rs.next()) {
					menu.setMenuid(rs.getInt("menuid"));
					menu.setMenuname(rs.getString("menuname"));
					menu.setMenuprice(rs.getDouble("menuprice"));
					menu.setMenutype(rs.getString("menutype"));
					
					
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return menu;
		}

	public void updateMenuPrice(int menuid,String menuname, double menuprice) {

        try {
        	
        	String query = "UPDATE menu SET  menuprice = ? WHERE menuid = ?";
        	ps = conn.prepareStatement(query);
        	
        	ps.setDouble(1, menuprice);
            ps.setInt(2, menuid);
            ps.executeQuery();
                		
	        
        } catch (SQLException ex) {
        	
            System.out.println("Update menu price failed: An Exception has occurred! " + ex);
            ex.printStackTrace();
            
        } finally {
        	
            try { 
            	
            	ps.close(); 
            	rs.close();  
            	
            } catch (Exception ignore) { }
            
        }
		
	}

}
