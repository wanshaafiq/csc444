package csc444.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import csc444.connection.ConnectionManager;
import csc444.model.orderListBean;
import csc444.model.orderdetailBean;

public class OrderdetailDAO {
	
	static Connection conn = null;
	static ResultSet rs = null;
	static PreparedStatement ps = null;
	static Statement stmt = null;

	public OrderdetailDAO() {
		conn = ConnectionManager.getConnection();
	}

	public static void  createorderDetail(orderdetailBean orderMenu) {
		
		int menuid= orderMenu.getMenuid();
		int orderid= orderMenu.getOrderid();
		int menuqty= orderMenu.getMenuqty();
		
		try {
			String query = "insert into orderdetail(orderid,menuid,menuqty) values(?,?,?)";
			
			ps = conn.prepareStatement(query);
			ps.setInt(1,orderid);
			ps.setInt(2,menuid);
			ps.setInt(3,menuqty);
			ps.executeUpdate();
				
		}
    	catch (Exception ex) {
    		System.out.println("failed: An Exception has occurred in create order Detail " + ex);
    	}
		
			
	}

	public static ArrayList<orderListBean> getAllOrderList(int orderid) {
		ArrayList<orderListBean> allOrderList = new ArrayList<orderListBean>();
		
		try {
			String query = "select * from menu join orderdetail on menu.menuid=orderdetail.menuid join orders on orderdetail.orderid=orders.orderid where orderstatus='PENDING' and orderpayment is null and orders.orderid=?";
			
			ps = conn.prepareStatement(query);
			ps.setInt(1,orderid);
			rs = ps.executeQuery();
			
			
			if(conn != null) {

	            // if user exists return true
	            while (rs.next()) {	
	            	
	            	orderListBean OrderList = new orderListBean();
	            	
	            	OrderList.setMenuid(rs.getInt("menuid"));
	            	OrderList.setMenuname(rs.getString("menuname"));
	            	OrderList.setMenuprice(rs.getDouble("menuprice"));
	            	OrderList.setMenuqty(rs.getInt("menuqty"));
	            	allOrderList.add(OrderList);     	       
	           	}
	            
	        } else {
	        
	        	System.out.println("Connection is null");
	        	
	        }		
		}
    	catch (Exception ex) {
    		System.out.println("failed: An Exception has occurred in get all order list " + ex);
    	}
		
		finally {
			try {
				ps.close();
				rs.close();
			} catch (Exception e) {

			}
		}
		
		return allOrderList;
		
	}

	public static double TotalPrice(ArrayList<orderListBean> OrderList) {
		
		double total=0.00;
		
		try {
			for(int i=0; i<OrderList.size(); i++) {
				String query = "select menuprice from menu where menuid=?";
				
				ps = conn.prepareStatement(query);
				ps.setInt(1, OrderList.get(i).getMenuid());
				rs = ps.executeQuery();
				
				if(rs.next()) {
										
					total += rs.getDouble("menuprice") * OrderList.get(i).getMenuqty();
					
				}
			}
		} catch (SQLException ex) {
        	
            System.out.println("Get total price by menus failed: An Exception has occurred! " + ex);
            ex.printStackTrace();
            
        } finally {
        	
            try { 
            	
            	ps.close(); 
            	rs.close();  
            	
            } catch (Exception ignore) { }
            
        }
		
		return total;
	}

	public static ArrayList<orderListBean> getAllOrderListReceipt(int orderid) {
		ArrayList<orderListBean> OrderList = new ArrayList<orderListBean>();

		try {
			String query = "select * from menu join orderdetail on menu.menuid=orderdetail.menuid join orders on orderdetail.orderid=orders.orderid where orderpayment is not null and orders.orderid=?";

			ps = conn.prepareStatement(query);
			ps.setInt(1, orderid);
			rs = ps.executeQuery();


			if (conn != null) {

				// if user exists return true
				while (rs.next()) {

					orderListBean List = new orderListBean();

					List.setMenuid(rs.getInt("menuid"));
					List.setMenuname(rs.getString("menuname"));
					List.setMenuprice(rs.getDouble("menuprice"));
					List.setMenuqty(rs.getInt("menuqty"));
					OrderList.add(List);

				}

			} else {

				System.out.println("Connection is null");

			}
		} catch (Exception ex) {
			System.out.println("failed: An Exception has occurred in get all order list for receipt " + ex);
		}

		finally {
			try {
				ps.close();
				rs.close();
			} catch (Exception e) {

			}
		}

		return OrderList;
	}

}
	

