package csc444.dao;


import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import csc444.connection.ConnectionManager;
import csc444.model.orderBean;
import csc444.model.orderhistoryBean;
import csc444.model.userBean;

public class OrderDAO {

	static Connection conn = null;
	static ResultSet rs = null;
	static PreparedStatement ps = null;
	static java.sql.Statement stmt = null;
	
	public OrderDAO() {
		conn = ConnectionManager.getConnection();
	}

	public static int checkorder(int userid) {
		
		conn = ConnectionManager.getConnection();
		orderBean order = new orderBean();
		
		try {
			
	    	   String searchQuery = "select * from orders where ORDERSTATUS='PENDING' and ORDERPAYMENT is null and userid=?";
	    	   
	           ps = conn.prepareStatement(searchQuery);
	       	   ps.setInt(1,userid); 
	           rs = ps.executeQuery();  
	 
				if (conn != null) {
					// if data exists
					if (rs.next()) 
					{
						order.setOrderid(rs.getInt("orderid"));
						
					}
				} else {
					System.out.println("connection is null");
				}
			}

	       catch (Exception ex) {
	           System.out.println("An Exception has occurred in check order " + ex);
	       }

	       finally {     
	          try {
	        	  ps.close();
	        	  rs.close();
	          }catch(Exception e){
	        	  
	          }
	       }

	       return order.getOrderid();
		}

	public static orderBean createOrder(int userid) {
		
		orderBean order = new orderBean();
		int newId = 0;
        try {
        	
        	String query = "INSERT INTO ORDERS(orderid, userid,orderstatus) VALUES(ORDER_SEQ.NEXTVAL, ?, ?)";
        	String generatedColumns[] = { "orderid" };
        	
        	ps = conn.prepareStatement(query, generatedColumns);
        	ps.setInt(1,userid);
            ps.setString(2,"PENDING");
            ps.executeUpdate();
	            
            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    newId = rs.getInt(1);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            
            order.setOrderid(newId);
            
            
        } catch (SQLException ex) {
        	
            System.out.println("An Exception has occurred in create order! " + ex);
            ex.printStackTrace();
            
        } finally {
        	
            try { 
            	
            	ps.close(); 
            	rs.close();  
            	
            } catch (Exception ignore) { }
            
        }
        
		return order;
   
    }

	public static void insertTimePromo(String time, int userid,int orderid,String promocode) {
	
	     try {
	    	   
	    	   String searchQuery = "update orders set ordertime=?, orderpromo=? where orderid=? and userid=?";
	    	   
	           ps = conn.prepareStatement(searchQuery);
	       	   ps.setString(1,time);
	       	   ps.setString(2, promocode);
	       	   ps.setInt(3,orderid);
	       	   ps.setInt(4,userid);
	           ps.executeUpdate();  
       
	 
			}

	       catch (Exception ex) {
	           System.out.println("An Exception has occurred in insert time & promo " + ex);
	       }

	       finally {     
	          try {
	        	  ps.close();
	        	  rs.close();
	          }catch(Exception e){
	        	  
	          }
	       }
	}

	public static void updateStatusCard(int userid, int orderid) {
	     try {
	    	   
	    	   String searchQuery = "update orders set orderstatus='PAID', orderpayment='Card' where orderid=? and userid=?";
	    	   
	           ps = conn.prepareStatement(searchQuery);
	       	   ps.setInt(1,orderid);
	       	   ps.setInt(2, userid);
	           ps.executeUpdate();  
	 
			}

	       catch (Exception ex) {
	           System.out.println("An Exception has occurred in update status card " + ex);
	       }

	       finally {     
	          try {
	        	  ps.close();
	        	  rs.close();
	          }catch(Exception e){
	        	  
	          }
	       }	
	}

	public static orderBean getOrderStatus(int userid, int orderid) {
		orderBean order = new orderBean();
		
		try {
			String query = "select * from orders where userid=? and orderid=?";

			ps = conn.prepareStatement(query);
			ps.setInt(1, userid);
			ps.setInt(2, orderid);
			rs = ps.executeQuery();
	
			if(rs.next()) {
				
				order.setOrdertime(rs.getString("ordertime"));
				order.setOrderstatus(rs.getString("orderstatus"));
				order.setOrderpayment(rs.getString("orderpayment"));
				order.setOrderpromo(rs.getString("orderpromo"));
			}

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
			} catch (Exception e) {

			}
		}
		
		return order;
	}

	public static void updateStatusCash(int userid, int orderid) {
		 try {
	    	   
	    	   String searchQuery = "update orders set orderpayment='Cash' where orderid=? and userid=?";
	    	   
	           ps = conn.prepareStatement(searchQuery);
	       	   ps.setInt(1,orderid);
	       	   ps.setInt(2, userid);
	           ps.executeUpdate();  
   	 
			}

	       catch (Exception ex) {
	           System.out.println("An Exception has occurred in update status cash " + ex);
	       }

	       finally {     
	          try {
	        	  ps.close();
	        	  rs.close();
	          }catch(Exception e){
	        	  
	          }
	       }		
	}

	public static userBean getUserdetail(int userid) {
		userBean user = new userBean();
		
		try {
			String query = "select * from users where userid=? ";

			ps = conn.prepareStatement(query);
			ps.setInt(1, userid);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				
				user.setName(rs.getString("name"));
				user.setAddress(rs.getString("address"));						
			}

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
			} catch (Exception e) {

			}
		}
		return user;
	}

	public static List<orderhistoryBean> custlistpendingOrder() {
		
		List<orderhistoryBean> updatestatus = new ArrayList<orderhistoryBean>();

		try {
			
			String query = "select * from orders where orderpayment IS NOT NULL and orderstatus='PENDING'";
			
			conn = ConnectionManager.getConnection();
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			

			while (rs.next()) {

				orderhistoryBean order = new orderhistoryBean();
				
				order.setUserid(rs.getInt("userid"));	
				order.setOrderid(rs.getInt("orderid"));	
				order.setOrdertime(rs.getString("ordertime"));
				order.setOrderstatus(rs.getString("orderstatus"));
				updatestatus.add(order);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return updatestatus;
	}

	public static void updatependingstatus(int orderid) {
		
		try {
	    	   
	    	   String searchQuery = "update orders set orderstatus=? where orderid=?";
	    	   
	           ps = conn.prepareStatement(searchQuery);
	       	   ps.setString(1,"PAID"); 
	       	   ps.setInt(2,orderid); 
	           ps.executeUpdate();  
    
			}

	       catch (Exception ex) {
	           System.out.println("An Exception has occurred in update user pending order status  " + ex);
	       }

	       finally {     
	          try {
	        	  ps.close();
	        	  rs.close();
	          }catch(Exception e){
	        	  
	          }
	       }
		
	}

	public static List<orderhistoryBean> listpaidOrder() {
		List<orderhistoryBean> listpaid = new ArrayList<orderhistoryBean>();

		try {
			
			String query = "select * from orders where orderstatus='PAID'";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				orderhistoryBean order = new orderhistoryBean();
				
				order.setUserid(rs.getInt("userid"));	
				order.setOrderid(rs.getInt("orderid"));	
				order.setOrdertime(rs.getString("ordertime"));
				order.setOrderstatus(rs.getString("orderstatus"));
				listpaid.add(order);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listpaid;
	}
	
	
}
